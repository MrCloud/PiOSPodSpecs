#
# Be sure to run `pod lib lint PiOSImageLoader.podspec' to ensure this is a
# valid spec before submitting.
#

Pod::Spec.new do |s|
  s.name             = 'PiOSImageLoader'
  s.version          = '0.9.2'
  s.summary          = 'Image loading made easy.'

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/mrcloud/PiOSImageLoader'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'mrcloud' => 'florianp37@me.com' }
  s.source           = { :git => 'https://gitlab.com/mrcloud/PiOSImageLoader.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'

#s.source_files = 'PiOSImageLoader/Classes/Core/**/*'

  s.default_subspec = 'Core'

  s.subspec 'Core' do |a|
    a.source_files = 'PiOSImageLoader/Classes/Core/**/*'
    #a.frameworks = 'QuartzCore'
    #a.weak_frameworks = 'AdSupport'
  end

  s.subspec 'KingFisher' do |a|
    a.source_files = 'PiOSImageLoader/Classes/KingFisher/**/*'
   a.dependency 'PiOSImageLoader/Core'
    a.dependency 'Kingfisher', '~> 3.5.0'
    a.xcconfig =  { 'OTHER_SWIFT_FLAGS' => '$(inherited) -DPiOSKingfisher' }
    #a.frameworks = 'QuartzCore'
    #a.weak_frameworks = 'AdSupport'
  end

  s.subspec 'Nuke' do |a|
    a.source_files = 'PiOSImageLoader/Classes/Nuke/**/*'
    a.dependency 'PiOSImageLoader/Core'
    a.dependency 'Nuke', '~> 5.1'
    a.xcconfig =  { 'OTHER_SWIFT_FLAGS' => '$(inherited) -DPiOSNuke' }
    #a.frameworks = 'QuartzCore'
    #a.weak_frameworks = 'AdSupport'
  end

  s.subspec 'SDWebImage' do |a|
    a.source_files = 'PiOSImageLoader/Classes/SDWebImage/**/*'
    a.dependency 'PiOSImageLoader/Core'
    a.dependency 'SDWebImage', '~> 4.0.0'
    a.xcconfig =  { 'OTHER_SWIFT_FLAGS' => '$(inherited) -DPiOSSDWebImage' }
    #a.frameworks = 'QuartzCore'
    #a.weak_frameworks = 'AdSupport'
  end

  # s.resource_bundles = {
  #   'PiOSImageLoader' => ['PiOSImageLoader/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
