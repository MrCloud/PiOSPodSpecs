#
# Be sure to run `pod lib lint PiOSNetworkClient.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'PiOSNetworkClient'
  s.version          = '0.1.0'
  s.summary          = 'PiOSNetworkClient is PiOS\' network client'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
PiOSNetworkClient enables anyone to simplify the way to consume webservices/APIs and map automagically the results to ObjectMapper\'s Mappable object instances
                       DESC

  s.homepage         = 'https://gitlab.com/mrcloud/PiOSNetworkClient'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'mrcloud' => 'florianp37@me.com' }
  s.source           = { :git => 'https://gitlab.com/mrcloud/PiOSNetworkClient.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.source_files = 'PiOSNetworkClient/Classes/**/*'

  # s.resource_bundles = {
  #   'PiOSNetworkClient' => ['PiOSNetworkClient/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
    s.dependency 'Moya', '8.0.3'
    s.dependency 'Moya-ObjectMapper', '2.3.1'
    s.dependency 'ObjectMapper', '2.2.6'
    s.dependency 'Alamofire', '4.4.0'
end
