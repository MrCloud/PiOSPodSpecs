#
# Be sure to run `pod lib lint PiOSDummyPod.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'PiOSDummyPod'
  s.version          = '0.1.3'
  s.summary          = 'A simple dummy pod to demonstrate subspecs usage.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
A simple dummy pod to demonstrate subspecs usage and library injection provided by PiOS
                       DESC

  s.homepage         = 'https://gitlab.com/mrcloud/PiOSDummyPod'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'mrcloud' => 'florianp37@me.com' }
  s.source           = { :git => 'https://gitlab.com/mrcloud/PiOSDummyPod.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'PiOSDummyPod/Classes/**/*'

  s.default_subspec = 'Core'

  s.subspec 'Core' do |a|
    a.source_files = 'PiOSDummyPod/Sources/Core/**/*'
    #a.dependency 'PiOSDummyPod/Core'
    #a.frameworks = 'QuartzCore'
    #a.weak_frameworks = 'AdSupport'
  end


  s.subspec 'ImplA' do |a|
    a.source_files = 'PiOSDummyPod/Sources/ImplA/**/*'
    a.dependency 'PiOSDummyPod/Core'
    a.xcconfig =  { 'OTHER_SWIFT_FLAGS' => '$(inherited) -DPiOSDummyImplA' }
    #a.frameworks = 'QuartzCore'
    #a.weak_frameworks = 'AdSupport'
  end

  s.subspec 'ImplB' do |a|
    a.source_files = 'PiOSDummyPod/Sources/ImplB/**/*'
    a.dependency 'PiOSDummyPod/Core'
    a.xcconfig =  { 'OTHER_SWIFT_FLAGS' => '$(inherited) -DPiOSDummyImplB' }
    #a.frameworks = 'QuartzCore'
    #a.weak_frameworks = 'AdSupport'
  end



  # s.resource_bundles = {
  #   'PiOSDummyPod' => ['PiOSDummyPod/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
