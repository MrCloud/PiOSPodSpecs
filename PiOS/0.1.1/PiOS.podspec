#
# Be sure to run `pod lib lint PiOS.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
s.name             = 'PiOS'
s.version          = '0.1.1'
s.summary          = 'The PiOS Framework.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

s.description      = <<-DESC
Hassle-free boilerplate and featurefull framework for your iOS apps
DESC

s.homepage         = 'https://gitlab.com/mrcloud/PiOSPod'
# s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
s.license          = { :type => 'MIT', :file => 'LICENSE' }
s.author           = { 'mrcloud' => 'florianp37@me.com' }
s.source           = { :git => 'https://gitlab.com/mrcloud/PiOSPod.git', :tag => s.version.to_s }
# s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

s.ios.deployment_target = '9.0'
s.source_files = 'PiOS/Classes/Core/**/*'
s.default_subspec = 'Core'

s.subspec 'Core' do |a|
a.source_files = 'PiOS/Classes/Core/**/*'
#a.frameworks = 'QuartzCore'
#a.weak_frameworks = 'AdSupport'
a.dependency 'PluggableApplicationDelegate'
end

end
