#
# Be sure to run `pod lib lint PiOSLogger.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'PiOSLogger'
  s.version          = '0.1.4'
  s.summary          = 'Addd Logging capabilities to your app with PiOSLogger.'

  s.description      = <<-DESC
Contains various loggers with different purposes: Dotzu, SwiftyBeaver, JustLog, CrashlyticsLogger, WatchDog.
                       DESC

  s.homepage         = 'https://gitlab.com/mrcloud/PiOSLogger'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'mrcloud' => 'florianp37@me.com' }
  s.source           = { :git => 'https://gitlab.com/mrcloud/PiOSLogger.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'

  s.source_files = 'PiOSLogger/Classes/Core/**/*'
  s.default_subspec = 'Core'

  s.subspec 'Core' do |a|
    a.source_files = 'PiOSLogger/Classes/Core/**/*'
  end

  s.subspec 'CrashlyticsLogger' do |a|
    a.source_files = 'PiOSLogger/Classes/CrashlyticsLogger/**/*'
    a.dependency 'PiOSLogger/Core'
    a.dependency 'CrashlyticsRecorder'
    a.xcconfig =  { 'OTHER_SWIFT_FLAGS' => '$(inherited) -DPiOSCrashlyticsLogger' }
    #a.frameworks = 'QuartzCore'
    #a.weak_frameworks = 'AdSupport'
  end

  s.subspec 'Watchdog' do |a|
    a.source_files = 'PiOSLogger/Classes/Watchdog/**/*'
    a.dependency 'PiOSLogger/Core'
    a.dependency 'Watchdog'
    a.xcconfig =  { 'OTHER_SWIFT_FLAGS' => '$(inherited) -DPiOSWatchdog' }
  end

  s.subspec 'Dotzu' do |a|
    a.source_files = 'PiOSLogger/Classes/Dotzu/**/*'
    a.dependency 'PiOSLogger/Core'
    a.dependency 'Dotzu'
    a.xcconfig =  { 'OTHER_SWIFT_FLAGS' => '$(inherited) -DPiOSDotzu' }
  end

  s.subspec 'SwiftyBeaver' do |a|
    a.source_files = 'PiOSLogger/Classes/SwiftyBeaver/**/*'
    a.dependency 'PiOSLogger/Core'
    a.dependency 'SwiftyBeaver'
    a.xcconfig =  { 'OTHER_SWIFT_FLAGS' => '$(inherited) -DPiOSSwiftyBeaver' }
  end

  s.subspec 'JustLog' do |a|
    a.source_files = 'PiOSLogger/Classes/JustLog/**/*'
    a.dependency 'PiOSLogger/Core'
    a.dependency 'JustLog'
    a.xcconfig =  { 'OTHER_SWIFT_FLAGS' => '$(inherited) -DPiOSJustLog' }
  end


  # s.resource_bundles = {
  #   'PiOSLogger' => ['PiOSLogger/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
